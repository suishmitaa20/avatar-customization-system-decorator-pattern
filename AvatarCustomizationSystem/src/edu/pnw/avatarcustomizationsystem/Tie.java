/**
 * @author suishmitaa srianand
 */

package edu.pnw.avatarcustomizationsystem;

/**
 * Class that extends the decorator class.
 */
public class Tie extends AvatarDecorator {
  /**
   * Instance of abstract Avatar class.
   */
  private Avatar avatar;

  @Override
  public String getDescription() {
    return "Tie, " + avatar.getDescription();
  }

  public Tie(Avatar avatar) {
    super();
    this.avatar = avatar;
  }
}
