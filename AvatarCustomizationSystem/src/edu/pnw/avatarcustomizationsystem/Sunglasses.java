/**
 * @author suishmitaa srianand
 */

package edu.pnw.avatarcustomizationsystem;

/**
 * Class that extends the decorator class.
 */
public class Sunglasses extends AvatarDecorator {
  /**
   * Instance of abstract Avatar class.
   */
  private Avatar avatar;

  @Override
  public String getDescription() {
    return "Sunglasses, " + avatar.getDescription();
  }

  public Sunglasses(Avatar avatar) {
    super();
    this.avatar = avatar;
  }
}
