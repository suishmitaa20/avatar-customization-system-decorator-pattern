/**
 * @author suishmitaa srianand
 */

package edu.pnw.avatarcustomizationsystem;

import java.util.Scanner;

/**
 * Implements the Avatar customization system.
 *
 */
public class AvatarCustomizationSystem {
  /**
   * Main method.
   * 
   * @param args Command line arguments.
   */
  public static void main(String[] args) {
    Scanner sin = new Scanner(System.in);
    Avatar avatar = null; // Empty object.
    System.out.println(
        "Welcome to the Avatar 1.0 System !\nPlease Select your character:\n1. Male\n2. Female");
    int gender = sin.nextInt();
    if (gender == 1) {
      avatar = new Male();
    } else if (gender == 2) {
      avatar = new Female();
    } else {
      System.out.println("Invalid option");
    }
    System.out.println("You have selected the " + avatar.getClass().getSimpleName() + ".");
    String custom = ""; // Customization option selected by the user.
    avatarSystem: // Labeled break.
    while (!custom.equals("exit")) {
      System.out.println(
          "Please select a cosmetic for your character:\n(Type �exit� to finish)\n\n        "
              + "1.  Jacket\n        2.  T-shirt\n        3.  Sweater\n        4.  Jeans\n      "
              + "  5.  Shorts\n        6.  Tie\n        7.  Neckless\n        8.  Earrings\n    "
              + "    9.  Sunglasses\n        10. Leather Shoes\n        11. High Heels\n        "
              + "12. Running Shoes\n");
      custom = sin.next();
      switch (custom) {
        case "1":
          avatar = new Jacket(avatar);
          break;
        case "2":
          avatar = new TShirt(avatar);
          break;
        case "3":
          avatar = new Sweater(avatar);
          break;
        case "4":
          avatar = new Jeans(avatar);
          break;
        case "5":
          avatar = new Shorts(avatar);
          break;
        case "6":
          avatar = new Tie(avatar);
          break;
        case "7":
          avatar = new Necklace(avatar);
          break;
        case "8":
          avatar = new Earrings(avatar);
          break;
        case "9":
          avatar = new Sunglasses(avatar);
          break;
        case "10":
          avatar = new LeatherShoes(avatar);
          break;
        case "11":
          avatar = new HighHeels(avatar);
          break;
        case "12":
          avatar = new RunningShoes(avatar);
          break;
        case "exit":
          break avatarSystem;
        default:
          break;
      }
      System.out.println(
          "You have selected " + avatar.getClass().getSimpleName() + " for your character.\n");
    }
    String a = avatar.getDescription();
    try {
      a = a.substring(0, a.length() - 2); // Removing the comma at the end.
      System.out.println("Your character has the following items:\n" + a);
    } catch (Exception e) {
      System.out.println("Your character has no items\n");
    }
    System.out.println("Thanks for using Avatar 1.0");
    sin.close();
  }
}
