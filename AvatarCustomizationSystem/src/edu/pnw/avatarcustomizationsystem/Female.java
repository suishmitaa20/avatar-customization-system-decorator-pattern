/**
 * @author suishmitaa srianand
 */

package edu.pnw.avatarcustomizationsystem;

/**
 * Inherits the abstract Avatar class.
 *
 */
public class Female extends Avatar {
  public Female() {
    setDescription("");
  }
}
