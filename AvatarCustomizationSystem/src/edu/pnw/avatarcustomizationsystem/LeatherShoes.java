/**
 * @author suishmitaa srianand
 */

package edu.pnw.avatarcustomizationsystem;

/**
 * Class that extends the decorator class.
 */
public class LeatherShoes extends AvatarDecorator {
  /**
   * Instance of abstract Avatar class.
   */
  private Avatar avatar;

  @Override
  public String getDescription() {
    return "Leather Shoes, " + avatar.getDescription();
  }

  public LeatherShoes(Avatar avatar) {
    super();
    this.avatar = avatar;
  }
}
