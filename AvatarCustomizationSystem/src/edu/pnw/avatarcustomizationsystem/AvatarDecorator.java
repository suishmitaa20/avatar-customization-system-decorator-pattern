/**
 * @author suishmitaa srianand
 *
 */

package edu.pnw.avatarcustomizationsystem;

/**
 * Abstract decorator pattern class.
 *
 */
abstract class AvatarDecorator extends Avatar {
  public abstract String getDescription();
}
