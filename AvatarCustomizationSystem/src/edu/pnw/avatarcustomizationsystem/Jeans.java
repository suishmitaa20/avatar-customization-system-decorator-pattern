/**
 * @author suishmitaa srianand
 */

package edu.pnw.avatarcustomizationsystem;

/**
 * Class that extends the decorator class.
 */
public class Jeans extends AvatarDecorator {
  /**
   * Instance of abstract Avatar class.
   */
  private Avatar avatar;

  @Override
  public String getDescription() {
    return "Jeans, " + avatar.getDescription();
  }

  public Jeans(Avatar avatar) {
    super();
    this.avatar = avatar;
  }
}
