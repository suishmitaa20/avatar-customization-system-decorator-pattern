/**
 * @author suishmitaa srianand
 */

package edu.pnw.avatarcustomizationsystem;

/**
 * Class that extends the decorator class.
 */
public class HighHeels extends AvatarDecorator {
  /**
   * Instance of abstract Avatar class.
   */
  private Avatar avatar;

  @Override
  public String getDescription() {
    return "High Heels, " + avatar.getDescription();
  }

  public HighHeels(Avatar avatar) {
    super();
    this.avatar = avatar;
  }
}
