/**
 * @author suishmitaa srianand
 */

package edu.pnw.avatarcustomizationsystem;

/**
 * An abstract class that describes the Avatar.
 *
 */
public abstract class Avatar {
  /**
   * Describes the characteristic of the class.
   */
  private String description;

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }
}
