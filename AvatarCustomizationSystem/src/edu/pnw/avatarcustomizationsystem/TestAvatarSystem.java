/**
 * @author suishmitaa srianand
 */

package edu.pnw.avatarcustomizationsystem;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

/**
 * Java JUnit test class.
 *
 */
class TestAvatarSystem {

  /**
   * Tests if the gender is assigned properly.
   */
  @Test
  void testGender() {
    Avatar a = new Male();
    assertEquals("", a.getDescription());

    a = new Female();
    assertEquals("", a.getDescription());
  }

  /**
   * Tests if the customizations are applied properly.
   */
  @Test
  void testApplyCustomization() {
    Avatar a = new Tie(new Male());
    assertEquals("Tie, ", a.getDescription());

    a = new Necklace(a);
    assertEquals("Necklace, Tie, ", a.getDescription());
  }
}
