/**
 * @author suishmitaa srianand
 *
 */

package edu.pnw.avatarcustomizationsystem;

/**
 * Class that extends the decorator class.
 */
public class Jacket extends AvatarDecorator {
  /**
   * Instance of abstract Avatar class.
   */
  private Avatar avatar;

  @Override
  public String getDescription() {
    return "Jacket, " + avatar.getDescription();
  }

  public Jacket(Avatar avatar) {
    super();
    this.avatar = avatar;
  }
}
