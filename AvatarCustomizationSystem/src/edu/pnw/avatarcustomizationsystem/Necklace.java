/**
 * @author suishmitaa srianand
 */

package edu.pnw.avatarcustomizationsystem;

/**
 * Class that extends the decorator class.
 */
public class Necklace extends AvatarDecorator {
  /**
   * Instance of abstract Avatar class.
   */
  private Avatar avatar;

  @Override
  public String getDescription() {
    return "Necklace, " + avatar.getDescription();
  }

  public Necklace(Avatar avatar) {
    super();
    this.avatar = avatar;
  }
}
