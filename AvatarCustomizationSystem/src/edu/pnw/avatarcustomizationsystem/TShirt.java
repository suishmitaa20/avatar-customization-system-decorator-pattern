/**
 * @author suishmitaa srianand
 */

package edu.pnw.avatarcustomizationsystem;

/**
 * Class that extends the decorator class.
 */
public class TShirt extends AvatarDecorator {
  /**
   * Instance of abstract Avatar class.
   */
  private Avatar avatar;

  @Override
  public String getDescription() {
    return "T-Shirt, " + avatar.getDescription();
  }

  public TShirt(Avatar avatar) {
    super();
    this.avatar = avatar;
  }
}
