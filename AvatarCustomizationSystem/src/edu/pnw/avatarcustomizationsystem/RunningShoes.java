/**
 * @author suishmitaa srianand
 */

package edu.pnw.avatarcustomizationsystem;

/**
 * Class that extends the decorator class.
 */
public class RunningShoes extends AvatarDecorator {
  /**
   * Instance of abstract Avatar class.
   */
  private Avatar avatar;

  @Override
  public String getDescription() {
    return "Running Shoes, " + avatar.getDescription();
  }

  public RunningShoes(Avatar avatar) {
    super();
    this.avatar = avatar;
  }
}
