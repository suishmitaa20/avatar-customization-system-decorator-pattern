/**
 * @author suishmitaa srianand
 */

package edu.pnw.avatarcustomizationsystem;

/**
 * Class that extends the decorator class.
 */
public class Sweater extends AvatarDecorator {
  /**
   * Instance of abstract Avatar class.
   */
  private Avatar avatar;

  @Override
  public String getDescription() {
    return "Sweater, " + avatar.getDescription();
  }

  public Sweater(Avatar avatar) {
    super();
    this.avatar = avatar;
  }
}
