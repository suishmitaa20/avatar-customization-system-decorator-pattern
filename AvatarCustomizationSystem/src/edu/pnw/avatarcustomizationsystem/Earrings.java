/**
 * @author suishmitaa srianand
 */

package edu.pnw.avatarcustomizationsystem;

/**
 * Class that extends the decorator class.
 */
public class Earrings extends AvatarDecorator {
  /**
   * Instance of abstract Avatar class.
   */
  private Avatar avatar;

  @Override
  public String getDescription() {
    return "Earrings, " + avatar.getDescription();
  }

  public Earrings(Avatar avatar) {
    super();
    this.avatar = avatar;
  }
}
