/**
 * @author suishmitaa srianand
 */

package edu.pnw.avatarcustomizationsystem;

/**
 * Class that extends the decorator class.
 */
public class Shorts extends AvatarDecorator {
  /**
   * Instance of abstract Avatar class.
   */
  private Avatar avatar;

  @Override
  public String getDescription() {
    return "Shorts, " + avatar.getDescription();
  }

  public Shorts(Avatar avatar) {
    super();
    this.avatar = avatar;
  }
}
